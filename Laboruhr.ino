#include "SDA5708.h"
#include "DS1302.h"

#define runEvery(t) for (static uint32_t _lasttime;\
    (uint32_t) millis() - _lasttime >= (t);\
    _lasttime += (t))

DateTime now;
SDA5708 display(5, 6, 7);
DS1302 rtc(4, 3, 2);

void setup() {
    DateTime setter;
    setter.hour = 18;
    setter.minute = 30;
    setter.second = 55;
    //setter.writeToRtc(rtc);

    now.readFromRtc(rtc);
}

void loop() {
    runEvery(1000) {
        now.tick();

        display.writeTwoDigitNumber(0, now.hour);
        display.writeTwoDigitNumber(3, now.minute);
        display.writeTwoDigitNumber(6, now.second);
    }

    runEvery(500) {
        uint8_t separator = millis() % 1000 < 500 ? ':' : ' ';
        display.writeChar(2, separator);
        display.writeChar(5, separator);
    }

    runEvery((uint32_t) 1000 * 60 * 10) {
        // read time from rtc every 10 minutes
        now.readFromRtc(rtc);
    }
}

